import type { LoginStrategyInterface } from '../interfaces/LoginStrategyInterface';
import { nsecBunkerLogin } from '../utils/helper';
import { ndkUser } from '../stores/stores';
import { UserProfileService } from './userProfileService';

export class BunkerLoginService implements LoginStrategyInterface {
	userProfileService = new UserProfileService();
	async login(nip46ConnectionString: string): Promise<'success' | 'error'> {
		try {
			const user = await nsecBunkerLogin(nip46ConnectionString);
			if (user) {
				ndkUser.set(user);
				this.userProfileService.fetchUserProfile(user.pubkey);
				return 'success';
			} else {
				console.error('nsec Bunker login failed.');
				return 'error';
			}
		} catch (error) {
			console.log('Error with nsec bunker login ' + error);
			return 'error';
		}
	}
}
